package sn.isepd.xml;

import org.jdom2.JDOMException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class Setup {
    public static void main(String[] args) throws JDOMException, IOException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        XmlWithJDomParser parser=new XmlWithJDomParser();
        List<Ecole> ecoles = parser.getAllData();
        new DatabaseUtils() .insetBatchData(ecoles);


    }

}
