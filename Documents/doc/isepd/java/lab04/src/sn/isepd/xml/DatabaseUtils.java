package sn.isepd.xml;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


import com.mysql.jdbc.Driver;
import com.mysql.jdbc.Driver.*;

public class DatabaseUtils {
    private static Connection getCOnnexion() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        try {



/*
            // chargement de la classe par son nom
            Class c = Class.forName("com.mysql.cj.jdbc.Driver");
            Driver pilote = (Driver) c.newInstance();
            // enregistrement du pilote auprès du DriverManager
            DriverManager.registerDriver(pilote);
            // Protocole de connexion
            String protocole = "jdbc:mysql:";
            // Adresse IP de l’hôte de la base et port
            String ip = "localhost";  // dépend du contexte
            String port = "3306";  // port MySQL par défaut
            // Nom de la base ;
            String nomBase = "isepd";  // dépend du contexte
            // Chaîne de connexion
            String conString = protocole + "//" + ip + ":" + port + "/" + nomBase;
            // Identifiants de connexion et mot de passe
            String nomConnexion = "isep";  // dépend du contexte
            String motDePasse = "isep";  // dépend du contexte
            // Connexion
            Connection connection = DriverManager.getConnection(conString, nomConnexion, motDePasse);*/


            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/isep?useSSL=false", "isep", "isep");

            return connection;

        } catch (Exception e) {
            System.out.println("########## can not open connection ####");
            throw e;
        }
    }

    private static void inserRowtData(Ecole ecole) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        //get connection

        try {
            Connection connection = getCOnnexion();
            Statement stmt = connection.createStatement();
            //étape 4: exécuter la requête
            System.out.println("Insertion des données...");
            String firstName = ecole.getFirstName();
            String lastName = ecole.getLastName();
            int older = ecole.getAge();
            String level = ecole.getLevel();
            String specilzation = ecole.getSpecialisation();

            String sql = "INSERT INTO ecole(nom, prenom, age, niveau, filiere) " +
                "VALUES ('" + firstName + "','" + lastName + "'," + older + ",'" + level + "','" + specilzation + "')";
            stmt.executeUpdate(sql);
            stmt.close();
            connection.close();
        } catch (Exception ex) {
            System.out.println("########## can not insert data into database ####" + ecole);
            throw ex;
        }

    }


    public void insetBatchData(List<Ecole> ecoles) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        if (ecoles.isEmpty()) return;
        for (Ecole ecole : ecoles) {
            inserRowtData(ecole);
        }

    }

}
