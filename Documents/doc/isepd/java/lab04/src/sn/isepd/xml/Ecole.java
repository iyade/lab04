package sn.isepd.xml;

public class Ecole {
    private String firstName, lastName, level, specialisation;
    private int age;

    public void Ecole() {
    }

    public Ecole(String firstName, String lastName, int age, String level, String specialisation) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.level = level;
        this.specialisation = specialisation;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Ecole{" +
            "firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", level='" + level + '\'' +
            ", specialisation='" + specialisation + '\'' +
            ", age=" + age +
            '}';
    }
}
