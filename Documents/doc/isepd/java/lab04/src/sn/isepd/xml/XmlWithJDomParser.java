package sn.isepd.xml;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class XmlWithJDomParser {
    private static final String FILENAME = "src/resources/ecole.xml";
    //private static final String REMOTE_URL = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

    public static void main(String[] args) throws JDOMException, IOException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        XmlWithJDomParser parser = new XmlWithJDomParser();
        parser.convertToEUro(1000.d, "XOF");

       /* List<Ecole> ecoles = parser.getAllData();
        new DatabaseUtils().insetBatchData(ecoles);*/


    }

    public List<Ecole> getAllData() throws JDOMException, IOException {
        List<Ecole> list = new ArrayList<>();
        try {

            SAXBuilder sax = new SAXBuilder();
            // XML is a local file
            Document doc = sax.build(new File(FILENAME));

            Element rootNode = doc.getRootElement();
            List<Element> classes = rootNode.getChildren("classe");

            for (Element classe : classes) {
                String level = classe.getAttributeValue("niveau");
                String filiere = classe.getAttributeValue("filiere");
                List<Element> students = classe.getChildren("etudiant");

                System.out.printf("niveau de la classe : %s%n", level);
                System.out.printf("filiere de la classe : %s%n", filiere);
                System.out.printf("################# debut classe ########################");
                for (Element target : students) {

                    String name = target.getChildText("nom");
                    String lastnae = target.getChildText("prenom");
                    String old = target.getChildText("age");


                    System.out.printf("nom : %s%n", name);
                    System.out.printf("prenom : %s%n", lastnae);
                    System.out.printf("age : %s%n", old);
                    Ecole ecole = new Ecole(name, lastnae, Integer.parseInt(old), level, filiere);
                    list.add(ecole);
                }
                System.out.printf("###############  fin  classe #######################");


            }
            return list;

        } catch (IOException | JDOMException e) {
            System.out.println("###### error " + e.getMessage());
            throw e;
        }
    }

    public Double convertToEUro(Double amount, String currency) throws JDOMException, IOException {


        try {

            SAXBuilder sax = new SAXBuilder();
            // XML is in a web-based location
            Document doc = sax.build(new URL(REMOTE_URL));

            Element rootNode = doc.getRootElement();

            System.out.println("######## root " + rootNode.getName());

            Element rootCube = rootNode.getChild("subject");

            Element cubeNode2 = rootCube.getChild("Cube");

            List<Element> curencies = cubeNode2.getChildren("Cube");
            for (Element level3 : curencies) {
                String devise = level3.getAttributeValue("currency");

                String taux = level3.getAttributeValue("rate");

                System.out.printf("Currency : %s%n", devise);
                System.out.printf("rate : %s%n", taux);


            }
            return 0.d;

        } catch (IOException | JDOMException e) {
            throw e;
        }
    }

}
